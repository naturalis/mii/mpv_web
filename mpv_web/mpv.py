import socket,os

class Mpv():
    """MPV class
    to connect to the local running mpv instance via socketfile
    """

    def __init__(self, whitelist, socketfile):
        self.whitelist = whitelist
        self.socketfile = socketfile
        self.socket = None
        self.connect()

    def connect(self):
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            self.socket.connect(self.socketfile)
        except:
            self.socket = None
            raise Exception("Socket '%s' does not exist, please start mpv" % (socketfile))
        return self.socket

    def send(self, jsoncommand):
        data = None
        if (self.connect()):
            self.socket.send(jsoncommand + b'\n')
            data = self.receive()
            self.socket.close()

        return data

    def receive(self):
        f = self.socket.makefile('r')
        data = f.readline()
        if data:
            return data

