from .__about__ import (
    __package_name__, __title__, __author__, __author_email__,
    __license__, __copyright__, __version__, __version_info__,
    __url__,
)

import socket, os
from flask import Flask, request, jsonify, json

def create_app(config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        WHITELIST=[
           'get_version',
           'get_property',
           'set_property',
           'property_list',
           'observe_property',
           'request_log_messages',
           'get_version',
           'seek',
           'keypress'
           'revert-seek',
           'frame-step',
           'frame-back-step',
           'screenshot',
           'screenshot-to-file',
           'playlist-clear',
           'playlist-next',
           'playlist-prev',
           'loadlist',
           'loadfile',
           'show-text'
            ],
        SOCKETFILE=os.environ.get('MPV_SOCKET','/tmp/mpvsocket')
    )

    if config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.update(test_config)

    from . import mpv

    mpvconnect = mpv.Mpv(
        socketfile=app.config['SOCKETFILE'],
        whitelist=app.config['WHITELIST']
    )

    @app.route("/")
    def mpv_howto():
        return '<b>No command?</b><br>Try for example: <a href="/get_property/playback-time">/get_property/playback-time'

    @app.route("/<string:command>")
    @app.route("/<string:command>/<string:params>")
    def mpv_command(command,params=''):
        jsonencoder = json.JSONEncoder()
        jsondecoder = json.JSONDecoder()
        try:
            mpvconnect.whitelist.index(command)
        except:
            return jsonify({'error' : "Command '%s' not allowed" % (command)})

        commandlist = {'command' : [command]}
        if (len(params)):
            commandlist['command'].append(params)
        try:
            result = mpvconnect.send(jsonencoder.encode(commandlist).encode())
        except Exception as reason:
            return jsonify({'error' : "Lost connection to mpv?", 'msg' : str(reason)})

        if (not result):
            return jsonify({'error' : "No result"})

        lines = result.decode().splitlines()
        decoded_result = []
        for line in lines :
            decoded_result.append(jsondecoder.decode(line))


        return jsonify(decoded_result)

    return app
