"""The module that holds much of the metadata about ppdb_nba."""
__package_name__ = 'mpv_web'
__title__ = 'Flask http wrapper for MPV'
__author__ = 'Naturalis Biodiversity Center'
__author_email__ = 'joep.vermaat@naturalis.nl'
__license__ = 'LGPL'
__copyright__ = 'Copyright 2018 Naturalis Biodiversity Center'
__version__ = '0.0.1'
__version_info__ = tuple(int(i) for i in __version__.split('.') if i.isdigit())
__url__ = 'https://github.com/MakeExpose/mpv_web'

__all__ = (
    '__package_name__', '__title__', '__author__', '__author_email__',
    '__license__', '__copyright__', '__version__', '__version_info__',
    '__url__',
)