# MPV-web

Dit is de mpv wrapper in flask om de socket communicatie met
een draaiende MPV te regelen via http. Het script gaat uit
van environment variabelen voor de socket en de whitelist
van commandos. 

## Socket

Dit is de socket waarmee gecommuniceerd wordt. Deze moet
worden gedefinieerd in de environment variabele `MPV_SOCKET`,
bijvoorbeeld `MPV_SOCKET=/tmp/mpvsocket`

## Whitelist

Omdat mpv ook allerlei commandos kan uitvoeren die bij voorkeur
niet door iedereen uitgevoerd mogen worden wordt een whitelist
aanbevolen. In instance/config.py staan de geaccepteerde commandos.

## Installeren

De flask applicatie is opgezet volgens [dit voorbeeld](https://github.com/pallets/flask/tree/1.0.2/examples/tutorial/flaskr).

Zodra je dit project met git gecloned hebt. Kan je het met pip installeren.

```
pip install -e .
```

of via github

```
pip install -e git+https://github.com/MakeExpose/mpv_web#egg=mpv_web
```

Eventueel door eerst een virual env omgeving op te zetten.

```
python3 -m venv venv
. venv/bin/activate
```



##  Uitproberen

Om deze setup te testen moet je de volgende stappen uitvoeren.
Zet de bovenstaande environment variabelen.

1. Start MPV

    ```
    export MPV_SOCKET=/tmp/mpvsocket 
    mpv --playlist=playlist.txt --loop-playlist --input-ipc-server=$MPV_SOCKET
    ```
    
Doe dit in productie bij voorkeur via een sytem service.

2. Start de flask webserver

    ```
    export FLASK_APP=mpv_web 
    flask run
    ```

3. Doe HTTP-GET calls naar de lokale webserver op poort 5000. bijvoorbeeld:

    `curl 'http://127.0.0.1:5000/loadfile/playlist.txt'`

    of

    `curl 'http://127.0.0.1:5000/get_property/playback-time'`
    
Andere commandos die geaccepteerd worden staan in de whitelist.
En staan ook beschreven in de [documentatie van mpv](https://mpv.io/manual/stable/#command-interface).


