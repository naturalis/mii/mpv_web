#!/bin/bash
export MPV_SOCKET=/tmp/mpvsocket
export FLASK_APP=mpv_web 
mpv --idle --playlist=playlist_testbeeld.txt --loop-playlist --input-ipc-server=$MPV_SOCKET &
flask run
