import setuptools

setuptools.setup(
      name='mpv_web',
      version='0.1',
      long_description=__doc__,
      description='Web wrapper for mpv',
      url='https://github.com/MakeExpose/mpv_web',
      author='Joep Vermaat',
      author_email='joep.vermaat@naturalis.nl',
      packages=['mpv_web'],
      include_package_data=True,
      install_requires=['Flask'],
      zip_safe=False,
      classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: LGPL License",
        "Operating System :: OS Independent",
      ),
)